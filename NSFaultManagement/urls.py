from django.urls import path, include
from rest_framework.routers import DefaultRouter
from NSFaultManagement.views import NSFaultManagementViewSet

router = DefaultRouter()
router.register(r'alarms', NSFaultManagementViewSet)

urlpatterns = [
    path('nsfm/v1/', include(router.urls)),
]
