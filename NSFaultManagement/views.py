from rest_framework import viewsets, status
from rest_framework.exceptions import APIException
from rest_framework.response import Response

from NSFaultManagement.models import Alarm
from NSFaultManagement.serializers import AlarmSerializer
from VIMManagement.utils.monitor_deployment import MonitorDeployment


class NSFaultManagementViewSet(viewsets.ModelViewSet):
    queryset = Alarm.objects.all()
    serializer_class = AlarmSerializer
    # monitor_deployment = MonitorDeployment()
    # monitor_deployment.watch_alarm()

    def create(self, request, *args, **kwargs):
        raise APIException(detail='Method Not Allowed',
                           code=status.HTTP_405_METHOD_NOT_ALLOWED)

    def update(self, request, *args, **kwargs):
        if 'AlarmModifications' not in request.data:
            raise APIException(detail='ValueError:Request need AlarmModifications',
                               code=status.HTTP_400_BAD_REQUEST)

        if 'ackState' not in request.data['AlarmModifications']:
            raise APIException(detail='ValueError:AlarmModifications need ackState',
                               code=status.HTTP_400_BAD_REQUEST)

        alarm = self.get_object()
        response = request.data.copy()
        request.data['ackState'] = request.data['AlarmModifications']['ackState']
        request.data['managedObjectId'] = alarm.managedObjectId
        super().update(request)
        return Response(response, status=status.HTTP_200_OK)

    def list(self, request, *args, **kwargs):
        """
            Query alarms related to NS instances.
            Get Alarm List. The client can use this method to retrieve information about the alarm list.
        """
        return super().list(request)

    def destroy(self, request, *args, **kwargs):
        raise APIException(detail='Method Not Allowed',
                           code=status.HTTP_405_METHOD_NOT_ALLOWED)
