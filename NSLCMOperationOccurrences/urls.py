from django.urls import path, include
from rest_framework.routers import DefaultRouter
from NSLCMOperationOccurrences import views

router = DefaultRouter()
router.register(r'ns_lcm_op_occs', views.NSLCMOperationOccurrencesViewSet)

urlpatterns = [
    path('nslcm/v1/', include(router.urls)),
]
