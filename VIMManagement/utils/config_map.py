from VIMManagement.utils.kubernetes_api import KubernetesApi


class ConfigMapClient(KubernetesApi):
    def __init__(self, *args, **kwargs):
        self.config_file_name = kwargs['config_file_name'] if 'config_file_name' in kwargs else None
        self.config_file_content = kwargs['config_file_content'] if 'config_file_content' in kwargs else None
        kwargs['instance_name'] = self.config_file_name if "." not in self.config_file_name else \
            self.config_file_name.split(".")[0]
        super().__init__(*args, **kwargs)

    def read_resource(self, **kwargs):
        return self.core_v1.read_namespaced_config_map(self.instance_name, self.namespace)

    def create_resource(self, **kwargs):
        self.core_v1.create_namespaced_config_map(self.namespace, self.resource)

    def patch_resource(self, **kwargs):
        self.resource.data = {self.config_file_name: self.config_file_content}
        self.core_v1.patch_namespaced_config_map(self.instance_name, self.namespace, self.resource)

    def delete_resource(self, **kwargs):
        self.core_v1.delete_namespaced_config_map(
            name=self.instance_name, namespace=self.namespace, body=self.delete_options)

    def instance_specific_resource(self, **kwargs):
        config_map = self.kubernetes_client.V1ConfigMap(api_version="v1", kind="ConfigMap")
        config_map.data = {self.config_file_name: self.config_file_content}
        config_map.metadata = self.kubernetes_client.V1ObjectMeta(name=self.instance_name)
        return config_map
