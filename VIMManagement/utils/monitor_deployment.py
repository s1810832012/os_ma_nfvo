import queue
import threading
import time
from functools import partial

from NSFaultManagement.utils.alarm_event import AlarmEvent
from VIMManagement.utils.base_kubernetes import BaseKubernetes
from utils.etcd_client.etcd_client import EtcdClient

is_running = False


class MonitorDeployment(BaseKubernetes):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.run_watch_event()
        self.etcd_client = EtcdClient()
        self.alarm = AlarmEvent()
        global is_running
        if not is_running:
            threading.Thread(
                target=self._get_replica_event,
                args=(self.app_v1.list_deployment_for_all_namespaces, self.deployment_status, self.pod_status),
                daemon=True
            ).start()
            threading.Thread(
                target=partial(self._get_pod_event),
                daemon=True
            ).start()
            threading.Thread(
                target=self._get_replica_event,
                args=(self.kubevirt_api.list_virtual_machine_instance_replica_set_for_all_namespaces,
                      self.virtual_machine_replica_set,
                      self.virtual_machine_status),
                daemon=True
            ).start()
            threading.Thread(
                target=partial(self._get_virtual_machine_event),
                daemon=True
            ).start()
        is_running = True

    # def _collect_resource_status(self):
    #     while True:
    #         self._get_replica_event(self.app_v1.list_deployment_for_all_namespaces,
    #                                 self.deployment_status,
    #                                 self.pod_status)
    #         self._get_pod_event()
    #         self._get_replica_event(self.kubevirt_api.list_virtual_machine_instance_replica_set_for_all_namespaces,
    #                                 self.virtual_machine_replica_set,
    #                                 self.virtual_machine_status)
    #         self._get_virtual_machine_event()

    # def run_watch_event(self):
    #     threading.Thread(
    #         target=partial(self._get_deployment_event),
    #         daemon=True
    #     ).start()
    #
    #     threading.Thread(
    #         target=partial(self._get_pod_event),
    #         daemon=True
    #     ).start()
    #
    #     threading.Thread(
    #         target=partial(self._get_vmirs_event),
    #         daemon=True
    #     ).start()
    #
    #     threading.Thread(
    #         target=partial(self._get_virtual_machine_event),
    #         daemon=True
    #     ).start()

    #
    # def _get_deployment_event(self):
    #     resource_version = None
    #     # while True:
    #     if resource_version is None:
    #         stream = self.watch.stream(partial(self.app_v1.list_deployment_for_all_namespaces), timeout_seconds=5)
    #     else:
    #         stream = self.watch.stream(partial(self.app_v1.list_deployment_for_all_namespaces),
    #                                    resource_version=resource_version, timeout_seconds=5)
    #
    #     for event in stream:
    #         _type = event['type']
    #         if 'name' not in event['object']['metadata']:
    #             continue
    #         _name = event['object']['metadata']['name']
    #         resource_version = event['object']['metadata']['resourceVersion']
    #         replicas = event['object']['spec']['replicas']
    #         if _type == 'DELETED' and _name in list(self.deployment_status):
    #             self.deployment_status.pop(_name)
    #         else:
    #             if _name not in list(self.deployment_status):
    #                 self.deployment_status[_name] = {'replicas': replicas}

    # def _get_vmirs_event(self):
    #     resource_version = None
    #     # while True:
    #     if resource_version is None:
    #         stream = self.watch.stream(
    #             partial(self.kubevirt_api.list_virtual_machine_instance_replica_set_for_all_namespaces),
    #             timeout_seconds=5)
    #     else:
    #         stream = self.watch.stream(
    #             partial(self.kubevirt_api.list_virtual_machine_instance_replica_set_for_all_namespaces),
    #             resource_version=resource_version, timeout_seconds=5)
    #
    #     for event in stream:
    #         _type = event['type']
    #         if 'name' not in event['object']['metadata']:
    #             continue
    #         _name = event['object']['metadata']['name']
    #         resource_version = event['object']['metadata']['resourceVersion']
    #         replicas = event['object']['spec']['replicas']
    #         if _type == 'DELETED' and _name in list(self.virtual_machine_replica_set):
    #             self.virtual_machine_replica_set.pop(_name)
    #         else:
    #             if _name not in list(self.virtual_machine_replica_set):
    #                 self.virtual_machine_replica_set[_name] = {'replicas': replicas}
    # def watch_alarm(self):
    #     threading.Thread(
    #         target=partial(self.loop_event),
    #         daemon=True
    #     ).start()
    #
    # def loop_event(self):
    #     while True:
    #         self._get_virtual_machine_event()
    #         self._get_pod_event()
    def _get_replica_event(self, events, record: dict, record_detail: dict):
        while True:
            stream = self.watch.stream(partial(events), timeout_seconds=5)

            for event in stream:
                _type = event['type']
                if 'name' not in event['object']['metadata']:
                    continue

                # resource_version = event['object']['metadata']['resourceVersion']
                _name = event['object']['metadata']['name']
                replicas = event['object']['spec']['replicas']
                # print(_type)
                # if _type == 'DELETED' and _name in list(record):
                #     record.pop(_name)
                # for record_detail_name in list(record_detail):
                #     if _name in record_detail_name:
                #         record_detail[record_detail_name] = 'Terminating'
                # else:
                if _name not in list(record):
                    record[_name] = {'replicas': replicas}

    def _get_virtual_machine_event(self):
        # resource_version = None
        # while True:
        # if resource_version is None:
        while True:
            for vmi in self.kubevirt_api.list_virtual_machine_instance_for_all_namespaces().items:
                metadata = vmi.metadata
                name = metadata.name
                deletion_timestamp = metadata.deletion_timestamp
                status = vmi.status
                phase = status.phase

                if status.conditions:
                    if 'Failed' == phase:
                        if not deletion_timestamp:
                            error_reason = None
                            error_message = None
                            type_record = list()
                            for conditions in status.conditions:
                                type_record.append(conditions.type)
                                if conditions.type != 'LiveMigratable' and conditions.type != 'Ready':
                                    error_reason = conditions.reason
                                    error_message = conditions.message
                            if 'Ready' not in type_record:
                                self.alarm.create_alarm(name, error_reason, error_message, False)
                                if name in list(self.virtual_machine_status):
                                    self.virtual_machine_status.pop(name)
                        elif name in list(self.virtual_machine_status):
                            self.virtual_machine_status.pop(name)
                    elif phase == 'Running' and name:
                        self.virtual_machine_status[name] = phase

    # else:
    #     stream = self.watch.stream(partial(self.kubevirt_api.list_virtual_machine_instance_for_all_namespaces),
    #                                resource_version=resource_version, timeout_seconds=5)
    # for event in stream:
    #     _type = event['type']
    #     _metadata = event['object']['metadata']
    #     _status = event['object']['status']
    #     if 'name' not in _metadata or 'phase' not in _status:
    #         continue
    #
    #     _name = _metadata['name']
    #     # resource_version = _metadata['resourceVersion']
    #     print(_type,_name,_status)
    #     if _type == 'ADDED':
    #         if 'phase' in _status and _status['phase'] == 'Running':
    #             self.virtual_machine_status[_name] = _status['phase']
    #     elif _type == 'MODIFIED':
    #         if 'Failed' == _status['phase'] and _name in list(
    #                 self.virtual_machine_status):
    #             self.virtual_machine_status.pop(_name)
    #             print('FailedFailedFailedFailed')
    #
    #         # if 'phase' in _status and 'deletionTimestamp' not in _metadata:
    #
    #             # if 'Failed' == _status['phase']:
    #             #     print('MODIFIEDMODIFIEDFailedFailed')
    #                 # self.alarm.create_alarm(_name, _status['conditions']['reason'],
    #                 #                         _status['conditions']['message'])
    #                 # if _name in list(self.virtual_machine_status):
    #                 #     self.virtual_machine_status.pop(_name)
    #         # else:
    #         #     if 'phase' in _status:
    #         #         print(_type, _name)
    #         #         if _status['phase'] == 'Failed':
    #         #             self.virtual_machine_status.pop(_name)
    #         #         else:
    #         #             self.virtual_machine_status[_name] = _status['phase']
    #     elif _type == 'DELETED':
    #         if 'Failed' == _status['phase'] and _name in list(
    #                 self.virtual_machine_status) and 'deletionTimestamp' in _metadata:
    #             print('FailedFailedFailed')
    #             # self.virtual_machine_status.pop(_name)
    #     print(self.virtual_machine_status)

    def _get_pod_event(self):
        resource_version = None
        while True:
            # if resource_version is None:
            stream = self.watch.stream(partial(self.core_v1.list_pod_for_all_namespaces), timeout_seconds=5)
            # else:
            #     stream = self.watch.stream(partial(self.core_v1.list_pod_for_all_namespaces),
            #                                resource_version=resource_version, timeout_seconds=5)

            for event in stream:
                _type = event['type']
                _metadata = event['object']['metadata']
                _status = event['object']['status']
                if 'name' not in _metadata:
                    continue

                _name = _metadata['name']
                resource_version = _metadata['resourceVersion']
                _phase = _status['phase']
                if _phase == 'Running':
                    self.pod_status[_name] = _phase

                if _type == 'MODIFIED' and 'deletionTimestamp' not in _metadata:
                    if 'containerStatuses' in _status:
                        container_status = _status['containerStatuses']
                        for status in container_status:
                            if 'state' not in status:
                                continue

                            state = status['state']
                            if 'waiting' in state and \
                                    'CrashLoopBackOff' == state['waiting']['reason']:
                                print('_get_pod_event_get_pod_event', _name)

                                self.alarm.create_alarm(_name, state['waiting']['reason'],
                                                        state['waiting']['message'], True)
                                if _name in list(self.pod_status):
                                    self.pod_crash_event(None, _name)

                elif _type == 'DELETED' and _name in list(self.pod_status) and 'deletionTimestamp' in _metadata:
                    self.pod_crash_event(_name, None)
                    # self.pod_status[_name] = 'Terminating'
                    self.pod_status.pop(_name)

            # print(self.pod_status)

    def pod_crash_event(self, instance_name, pod_name):
        self.etcd_client.set_deploy_name(instance_name=instance_name, pod_name=pod_name)
        self.etcd_client.release_pod_ip_address()

    def watch_specific_deployment(self, container_instance_name, vm_instance_name, _status, events):
        _queue = queue.Queue()
        # check two event
        success_count = 2
        threading.Thread(
            target=partial(self._get_deploy_status, _queue=_queue, events=events, success_count=success_count),
            daemon=True
        ).start()

        threading.Thread(
            target=lambda q, deployment_names, status: q.put(
                self._check_status(input_set=deployment_names, status=status, isContainer=True)),
            args=(_queue, container_instance_name, _status),
            daemon=True
        ).start()

        threading.Thread(
            target=lambda q, vm_names, status: q.put(
                self._check_status(input_set=vm_names, status=status, isContainer=False)),
            args=(_queue, vm_instance_name, _status),
            daemon=True
        ).start()

    def _check_status(self, input_set, status, isContainer):
        loop_count = -1
        while len(input_set) != 0:
            if isContainer:
                all_resource = self.deployment_status
                server_set = self.pod_status
            else:
                all_resource = self.virtual_machine_replica_set
                server_set = self.virtual_machine_status

            if loop_count < 0:
                loop_count = len(input_set) - 1

            specific_input_resource = list(input_set)[loop_count]
            # if status == 'Terminating':
            #     if specific_input_resource not in list(all_resource):
            #         input_set.remove(specific_input_resource)
            # else:
            # print(all_resource, 'input_setinput_setall_resourceall_resource')
            # print(list(all_resource))
            if specific_input_resource in list(all_resource):
                if status == 'Terminating':
                    if not any(specific_input_resource in _ for _ in list(server_set)):
                        input_set.remove(specific_input_resource)
                        all_resource.pop(specific_input_resource)
                else:
                    complete_count = 0
                    for name in list(server_set):
                        if specific_input_resource in name:
                            if status == server_set[name]:
                                complete_count += 1
                    if all_resource[specific_input_resource]['replicas'] == complete_count:
                        input_set.remove(specific_input_resource)

            loop_count -= 1
        return True

    # def _check_specific_vm_status(self, input_vm_set, status):
    #     loop_count = -1
    #     while len(input_vm_set) != 0:
    #         self._get_replica_event(self.kubevirt_api.list_virtual_machine_instance_replica_set_for_all_namespaces,
    #                                 self.virtual_machine_replica_set)
    #         self._get_virtual_machine_event()
    #
    #         if loop_count < 0:
    #             loop_count = len(input_vm_set) - 1
    #
    #         virtual_machine_replica_dict = list(self.virtual_machine_replica_set)
    #         input_specific_vm = list(input_vm_set)[loop_count]
    #         if status == 'Terminating':
    #             if input_specific_vm not in virtual_machine_replica_dict:
    #                 input_vm_set.remove(input_specific_vm)
    #         else:
    #             if input_specific_vm in virtual_machine_replica_dict:
    #                 all_vm_running_count = 0
    #                 for vm_name in list(self.virtual_machine_status):
    #                     if input_specific_vm in vm_name:
    #                         if status == self.virtual_machine_status[vm_name]:
    #                             all_vm_running_count += 1
    #                 if self.virtual_machine_replica_set[input_specific_vm]['replicas'] == all_vm_running_count:
    #                     input_vm_set.remove(input_specific_vm)
    #
    #         loop_count -= 1
    #     return True
    #
    # def _check_specific_deployment_status(self, input_deployment_set, status):
    #     loop_count = -1
    #     while len(input_deployment_set) != 0:
    #         self._get_replica_event(self.app_v1.list_deployment_for_all_namespaces, self.deployment_status)
    #         # self._get_deployment_event()
    #         self._get_pod_event()
    #
    #         if loop_count < 0:
    #             loop_count = len(input_deployment_set) - 1
    #
    #         all_deployment_key = list(self.deployment_status)
    #         input_specific_deployment = list(input_deployment_set)[loop_count]
    #         if status == 'Terminating':
    #             if input_specific_deployment not in all_deployment_key:
    #                 input_deployment_set.remove(input_specific_deployment)
    #         else:
    #             if input_specific_deployment in all_deployment_key:
    #                 all_pod_running_count = 0
    #                 for pod_name in list(self.pod_status):
    #                     if input_specific_deployment in pod_name:
    #                         if status == self.pod_status[pod_name]:
    #                             all_pod_running_count += 1
    #                 if self.deployment_status[input_specific_deployment]['replicas'] == all_pod_running_count:
    #                     input_deployment_set.remove(input_specific_deployment)
    #
    #         loop_count -= 1
    #     return True

    def _get_deploy_status(self, _queue, events, success_count):
        while success_count:
            success_status = _queue.get()
            if success_status:
                success_count -= 1
        print('------------successsuccesssuccesssuccesssuccesssuccesssuccesssuccesssuccesssuccesssuccess------------')
        [event() for event in events]
