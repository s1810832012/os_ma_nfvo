from VIMManagement.utils.kubernetes_api import KubernetesApi


class PersistentVolumeClaimClient(KubernetesApi):
    def __init__(self, *args, **kwargs):
        if 'storage_size' in kwargs:
            self.storage_size = kwargs['storage_size']
        super().__init__(*args, **kwargs)

    def read_resource(self, **kwargs):
        return self.core_v1.read_namespaced_persistent_volume_claim(self.instance_name, self.namespace)

    def create_resource(self, **kwargs):
        self.core_v1.create_namespaced_persistent_volume_claim(self.namespace, self.resource)

    def patch_resource(self, **kwargs):
        self.core_v1.patch_namespaced_persistent_volume_claim(self.instance_name, self.namespace, self.resource)

    def delete_resource(self, **kwargs):
        self.core_v1.delete_namespaced_persistent_volume_claim(
            name=self.instance_name, namespace=self.namespace, body=self.delete_options)

    def instance_specific_resource(self, **kwargs):
        persistent_volume_claim = self.kubernetes_client.V1PersistentVolumeClaim(
            api_version='v1', kind='PersistentVolumeClaim')
        persistent_volume_claim.metadata = self.kubernetes_client.V1ObjectMeta(name=self.instance_name)
        persistent_volume_claim.spec = self.kubernetes_client.V1PersistentVolumeClaimSpec(
            access_modes=["ReadWriteOnce"], resources={"requests": {"storage": self.storage_size}},
            selector={"matchLabels": {"name": self.instance_name}})
        return persistent_volume_claim
