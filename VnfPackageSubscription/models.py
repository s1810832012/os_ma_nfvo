import uuid
from django.db import models


class PkgmSubscription(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    callbackUri = models.TextField()


class PkgmSubscriptionLink(models.Model):
    _links = models.OneToOneField(PkgmSubscription,
                                  on_delete=models.CASCADE,
                                  primary_key=True,
                                  related_name='PkgmSubscription_links')
    link_self = models.URLField()


class PkgmNotificationsFilter(models.Model):
    filter = models.OneToOneField(PkgmSubscription,
                                  on_delete=models.CASCADE,
                                  primary_key=True,
                                  related_name='PkgmSubscription_filter')
    notificationTypes = models.TextField(null=True, blank=True)
    vnfdId = models.TextField(null=True, blank=True)
    vnfPkgId = models.TextField(null=True, blank=True)
    operationalState = models.TextField(null=True, blank=True)
    usageState = models.TextField(null=True, blank=True)


class PkgmNotificationsProviders(models.Model):
    vnfProductsFromProviders = models.ForeignKey(PkgmNotificationsFilter,
                                                 on_delete=models.CASCADE,
                                                 null=True,
                                                 blank=True,
                                                 related_name='PkgmSubscription_providers')
    vnfProvider = models.TextField()


class PkgmNotificationsProducts(models.Model):
    vnfProducts = models.ForeignKey(PkgmNotificationsProviders,
                                    on_delete=models.CASCADE,
                                    null=True,
                                    blank=True,
                                    related_name='PkgmSubscription_products')
    vnfProductName = models.TextField()


class PkgmNotificationsVersions(models.Model):
    versions = models.ForeignKey(PkgmNotificationsProducts,
                                 on_delete=models.CASCADE,
                                 null=True,
                                 blank=True,
                                 related_name='PkgmSubscription_versions')
    vnfSoftwareVersion = models.TextField()
    vnfdVersions = models.TextField(null=True, blank=True)
