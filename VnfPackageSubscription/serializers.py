import json
from rest_framework import serializers

from utils.format_tools import transform_representation
from .models import *


class PkgmSubscriptionLinkSerializer(serializers.ModelSerializer):
    self = serializers.CharField(source='link_self')

    class Meta:
        model = PkgmSubscriptionLink
        fields = ('self',)


class PkgmNotificationsVersionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PkgmNotificationsVersions
        fields = ('vnfSoftwareVersion', 'vnfdVersions')

    def to_representation(self, instance):
        return transform_representation(super().to_representation(instance))


class PkgmNotificationsProductsSerializer(serializers.ModelSerializer):
    versions = PkgmNotificationsVersionsSerializer(
        many=True, required=False, source='PkgmSubscription_versions')

    class Meta:
        model = PkgmNotificationsProducts
        fields = ('vnfProductName', 'versions')


class PkgmNotificationsProvidersSerializer(serializers.ModelSerializer):
    vnfProducts = PkgmNotificationsProductsSerializer(
        many=True, required=False, source='PkgmSubscription_products')

    class Meta:
        model = PkgmNotificationsProviders
        fields = ('vnfProvider', 'vnfProducts')


class PkgmNotificationsFilterSerializer(serializers.ModelSerializer):
    vnfProductsFromProviders = PkgmNotificationsProvidersSerializer(required=False, many=True,
                                                                    source='PkgmSubscription_providers')

    class Meta:
        model = PkgmNotificationsFilter
        fields = ('notificationTypes', 'vnfProductsFromProviders', 'vnfdId', 'vnfPkgId',
                  'operationalState', 'usageState')

    def to_representation(self, instance):
        return transform_representation(super().to_representation(instance))


class PkgmSubscriptionSerializer(serializers.ModelSerializer):
    _links = PkgmSubscriptionLinkSerializer(source='PkgmSubscription_links')
    filter = PkgmNotificationsFilterSerializer(required=False, source='PkgmSubscription_filter')

    class Meta:
        model = PkgmSubscription
        fields = '__all__'

    def create(self, validated_data):
        filter_value = validated_data.pop('PkgmSubscription_filter', None)
        link_value = validated_data.pop('PkgmSubscription_links')
        pkgm = PkgmSubscription.objects.create(**validated_data)
        PkgmSubscriptionLink.objects.create(_links=pkgm,
                                            **{'link_self': link_value['link_self'] + str(pkgm.id)})

        if filter_value:
            pkgm_providers_list = filter_value.pop('PkgmSubscription_providers', None)
            pkgm_filter = PkgmNotificationsFilter.objects.create(filter=pkgm, **filter_value)
            if pkgm_providers_list:
                for providers in pkgm_providers_list:
                    products_list = providers.pop('PkgmSubscription_products', None)
                    pkgm_providers = pkgm_filter.PkgmSubscription_providers.create(**providers)
                    if products_list:
                        for products in products_list:
                            versions_list = products.pop('PkgmSubscription_versions', None)
                            pkgm_products = pkgm_providers.PkgmSubscription_products.create(**products)
                            if versions_list:
                                for versions in versions_list:
                                    pkgm_products.PkgmSubscription_versions.create(**versions)
        return pkgm
