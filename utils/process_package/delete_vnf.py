from VIMManagement.utils.circuit_breaking import CircuitBreakingClient
from VIMManagement.utils.config_map import ConfigMapClient
from VIMManagement.utils.deployment import DeploymentClient
from VIMManagement.utils.horizontal_pod_autoscaler import HorizontalPodAutoscalerClient
from VIMManagement.utils.persistent_volume import PersistentVolumeClient
from VIMManagement.utils.persistent_volume_claim import PersistentVolumeClaimClient
from VIMManagement.utils.retry_policy import RetryPolicyClient
from VIMManagement.utils.service import ServiceClient
from VIMManagement.utils.virtual_machine_instance import VirtualMachineInstance
from os_ma_nfvo import settings
from utils.file_manipulation import remove_file
from utils.process_package.process_vnf_instance import ProcessVNFInstance


class DeleteService(ProcessVNFInstance):
    def __init__(self, package_id, vnf_instance_name):
        super().__init__(package_id, vnf_instance_name)

    def process_config_map(self, **kwargs):
        client = ConfigMapClient(
            instance_name=self.vnf_instance_name, namespace=kwargs['namespace'],
            config_file_name=kwargs['artifacts_name'])
        client.handle_delete()

    def process_deployment(self, **kwargs):
        data = {'instance_name': self.vnf_instance_name, 'namespace': kwargs['vdu_info']['namespace']}
        if kwargs['vdu_info']['diskFormat'] == 'raw':
            client = DeploymentClient(**data)
        else:
            client = VirtualMachineInstance(**data)

        client.handle_delete()

    def process_service(self, **kwargs):
        client = ServiceClient(
            instance_name=kwargs['vdu'].attributes['name_of_service'],
            namespace=kwargs['vdu'].attributes['namespace'])
        client.handle_delete()

    def process_persistent_volume_claim(self, **kwargs):
        client = PersistentVolumeClaimClient(
            instance_name=self.vnf_instance_name, namespace=kwargs['vdu'].attributes['namespace'])
        client.handle_delete()

    def process_persistent_volume(self, **kwargs):
        vdu = kwargs['vdu']
        client = PersistentVolumeClient(instance_name=self.vnf_instance_name)
        if vdu.requirements['type_of_storage'] == 'nfs':
            remove_file("{}{}".format(settings.NFS_PATH, self.vnf_instance_name))
            client.handle_delete()
        elif vdu.requirements['type_of_storage'] == 'volume' or vdu.requirements['type_of_storage'] == 'local':
            remove_file("{}{}".format(settings.HOST_PATH, self.vnf_instance_name))
            client.handle_delete()
        else:
            raise APIException(detail='storage type only local or nfs',
                                   code=status.HTTP_409_CONFLICT)

    def process_horizontal_pod_autoscaler(self, **kwargs):
        client = HorizontalPodAutoscalerClient(
            instance_name=self.vnf_instance_name, namespace=kwargs['vdu'].attributes['namespace'],
            isContainer=kwargs['isContainer'])
        client.handle_delete()

    def process_service_mesh_circuit_breaking(self, **kwargs):
        vdu = kwargs['vdu']
        if kwargs['circuit_breaking']:
            circuit_breaking = kwargs['circuit_breaking']

            client = CircuitBreakingClient(
                circuit_breaking_name=self.vnf_instance_name + '-circuit-breaking',
                circuit_breaking_host=vdu.attributes['name_of_service'],
                circuit_breaking_maxConnections=circuit_breaking[0],
                circuit_breaking_maxRequestsPerConnection=circuit_breaking[1])
            client.delete_resource()
    def process_service_mesh_retry_policy(self, **kwargs):
        vdu = kwargs['vdu']
        if kwargs['retry_policy']:
            retry_policy = kwargs['retry_policy']
            # 建立重試機制api
            client = RetryPolicyClient(
                retry_policy_name=self.vnf_instance_name+'-retry-policy',
                retry_policy_host=self.vnf_instance_name,
                retry_policy_attempts=retry_policy[0],
                retry_policy_perTryTimeout=retry_policy[1])
            client.delete_resource()
