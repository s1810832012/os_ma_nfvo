from utils.tosca_paser.entity_template import EntityTemplate

CNI_TYPE = (SR_IOV, OVS) = ('sr-iov', 'ovs')


class CPTemplate(EntityTemplate):
    CP_PROPERTIES = (LAYER_PROTOCOL, CP_TYPE) = ('layer_protocol', 'type')
    CP_REQUIREMENTS = (VIRTUAL_BINDING, VIRTUAL_LINK) = ('virtual_binding', 'virtual_link')

    def __init__(self, node_template, name):
        super().__init__(node_template, name)
        self.properties = self._get_properties(self.CP_PROPERTIES)
        self.requirements = self._get_requirements(self.CP_REQUIREMENTS)

    def _validate_properties(self):
        if self.PROPERTIES not in self.template:
            self._value_empty_exception('cp', self.PROPERTIES)

        properties = self.template.get(self.PROPERTIES)
        if self.LAYER_PROTOCOL not in properties:
            self._value_empty_exception('cp properties', self.LAYER_PROTOCOL)

        return True

    def _validate_requirements(self):
        if self.REQUIREMENTS not in self.template:
            self._value_empty_exception('cp', self.REQUIREMENTS)

        requirements = self.template.get(self.REQUIREMENTS)
        if not requirements or self.VIRTUAL_BINDING not in requirements \
                or self.VIRTUAL_LINK not in requirements:
            self._value_empty_exception('cp requirements', '{} and {}'.format(self.VIRTUAL_BINDING, self.VIRTUAL_LINK))

        if self.CP_TYPE in self.REQUIREMENTS:
            if self.REQUIREMENTS[self.CP_TYPE] not in self.CNI_TYPE:
                self._value_error_exception(self.REQUIREMENTS, self.CP_TYPE)

        return True

    def _validate_artifacts(self):
        pass

    def _validate_attributes(self):
        pass

    def _validate_capabilities(self):
        pass
