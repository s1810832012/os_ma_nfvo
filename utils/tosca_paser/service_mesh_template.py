from utils.tosca_paser.entity_template import EntityTemplate


class SMTemplate(EntityTemplate):
    # CB_PROPERTIES = (MAXCONNECTIONS, MAXREQUESTSPERCONNECTION, ATTEMPTS, PERTRYTIMEOUT, RETRYON) = \
    #     ('maxConnections', 'maxRequestsPerConnection', 'attempts', 'perTryTimeout', 'retryOn')
    CB_PROPERTIES = (MAXCONNECTIONS, MAXREQUESTSPERCONNECTION) = \
        ('maxConnections', 'maxRequestsPerConnection')
    RP_PROPERTIES = (ATTEMPTS, PERTRYTIMEOUT, RETRYON) = \
        ('attempts', 'perTryTimeout', 'retryOn')

    def __init__(self, node_template, name):
        super().__init__(node_template, name)
        self.cb_properties = self._get_properties(self.CB_PROPERTIES)
        self.rp_properties = self._get_properties(self.RP_PROPERTIES)

    def _validate_properties(self):
        if self.PROPERTIES not in self.template:
            self._value_empty_exception('service mesh', self.PROPERTIES)

        properties = self.template.get(self.PROPERTIES)
        if 'circuit_breaking' not in properties or not isinstance(properties.get('circuit_breaking'), dict):
            self._value_error_exception('sm properties', 'circuit_breaking')

        circuit_breaking = properties.get('circuit_breaking')
        for cb_properties in self.CB_PROPERTIES:
            if cb_properties not in circuit_breaking:
                self._value_empty_exception('circuit breaking properties', cb_properties)
            # print(cb_properties)

        retry_policy = properties.get('retry_policy')
        for rp_properties in self.RP_PROPERTIES:
            if rp_properties not in retry_policy:
                self._value_empty_exception('retry policy properties', cb_properties)
            # print(rp_properties)

        return True

    def _validate_artifacts(self):
        pass

    def _validate_attributes(self):
        pass

    def _validate_requirements(self):
        pass

    def _validate_capabilities(self):
        pass