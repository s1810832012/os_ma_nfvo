from utils.tosca_paser.entity_template import EntityTemplate


class VNFFGTemplate(EntityTemplate):
    VNFFG_PROPERTIES = (ID, VENDOR, VERSION, NUMBER_OF_ENDPOINTS) = \
        ('id', 'vendor', 'version', 'number_of_endpoints')
    VNFFG_PROPERTIES_LIST = (DEPENDENT_VIRTUAL_LINK, CONNECTION_POINT, CONSTITUENT_VNFS) = \
        ('dependent_virtual_link', 'connection_point', 'constituent_vnfs')
    VNFFG_TARGETS = (TARGETS) = 'targets'

    def __init__(self, node_template, name):
        super().__init__(node_template, name)
        self.properties = self._get_properties(properties=self.VNFFG_PROPERTIES,
                                               properties_list=self.VNFFG_PROPERTIES_LIST)
        self.targets = self.template.get(self.TARGETS)

    def _validate_properties(self):
        if self.PROPERTIES not in self.template:
            self._value_empty_exception('vnffg', self.PROPERTIES)

        properties = self.template.get(self.PROPERTIES)
        for vnffg_property in properties:
            if vnffg_property not in self.VNFFG_PROPERTIES and vnffg_property not in self.VNFFG_PROPERTIES_LIST:
                self._value_error_exception('vnffg', vnffg_property)

        return True

    def _validate_artifacts(self):
        pass

    def _validate_attributes(self):
        pass

    def _validate_requirements(self):
        pass

    def _validate_capabilities(self):
        pass
